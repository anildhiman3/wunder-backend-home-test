
Wunder Mobility Home Test
-------------------

1. Describe possible performance optimizations for your Code.
   Code is simple neat and clean and it will run perfectly fine. I have followed all coding standard provided by
   Yii2 itself, MVC pattern, Active query, User Identity, Model validation and scenarios, Migration, layout, before and after 
   save events, time stamp behaviour and attribute behaviors, Expression and many more things which are provided by Yii2 itself.
   

2. Which things could be done better, than you’ve done it?

    Every Developer has its own way or structure of coding, As I like to follow coding standard provided by framework itself
    either Yii, Cakephp, codeigniter, symphony or express or angularjs or whatever tech. we use to developer frontend/backend

    If any other developer will work then either he/she will customise the structure of its own way/ or they can integrate 
    test case and they can use session or cookie to main state management as i have use DB for it or etc.
   
    Current there are lots of things which could be done in better way in this system. this is just a test so I followed 
    the instruction but if we can give more time than we can create test cases, more precise validations, and good 
    bootstrap design, for security captcha , email and many more.
   
    I am just not able to integrate the api request reason being its giving me authentication error


Installation Instruction
-------------------
1. run "composer up" command in root directly
2. then run command "php init" and select Env. (0 is pref. if not live)
3. set database credentials in common > config >  main-local.php (localhost:port/database/user/password)
4. run command "php yii migration/run" (only in case if we are not using sql file and just doing fresh installation)

    
DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```

<?php

namespace common\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\IdentityInterface;
use yii\httpclient\Client;
/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $user_uuid
 * @property string $unique_login_id
 * @property string $firstname
 * @property string $lastname
 * @property string $telephone
 * @property string $house_number
 * @property string $street
 * @property string $zip_code
 * @property string $city
 * @property string $account_owner
 * @property string $iban
 * @property string|null $paymentDataId
 * @property int $stage_completed 1-personal,2-address,3-payment
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const SCENARIO_PERSONAL = 'scenario_personal';
    const SCENARIO_ADDRESS = 'scenario_address';
    const SCENARIO_PAYMENT = 'scenario_payment';

    const STAGE_COMPLETED_INITIAL = 0;
    const STAGE_COMPLETED_PERSONAL = 1;
    const STAGE_COMPLETED_ADDRESS = 2;
    const STAGE_COMPLETED_PAYMENT = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unique_login_id', 'firstname', 'lastname', 'telephone', 'house_number', 'street', 'zip_code', 'city', 'account_owner', 'iban'], 'required'],
            [['stage_completed'], 'integer'],
            [['created_at', 'updated_at','user_uuid'], 'safe'],
            [['user_uuid'], 'string', 'max' => 60],
            [['unique_login_id'], 'string', 'max' => 35],
            [['firstname'], 'string', 'max' => 255],
            [['lastname'], 'string', 'max' => 32],
            [['telephone', 'house_number', 'zip_code', 'city', 'iban'], 'string', 'max' => 10],
            [['street', 'account_owner', 'paymentDataId'], 'string', 'max' => 100],
            [['user_uuid'], 'unique'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_PERSONAL] = ['firstname', 'lastname', 'telephone'];
        $scenarios[self::SCENARIO_ADDRESS] =  ['house_number', 'street', 'zip_code', 'city'];
        $scenarios[self::SCENARIO_PAYMENT] =  ['account_owner', 'iban'];
        return $scenarios;
    }

    /**
     * @return array
     */
    public function behaviors() {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'user_uuid',
                ],
                'value' => function() {
                    if (!$this->user_uuid)
                        $this->user_uuid = new Expression('UUID()');

                    return $this->user_uuid;
                }
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_uuid' => 'User Uuid',
            'unique_login_id' => 'Unique Login ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'telephone' => 'Telephone',
            'house_number' => 'House Number',
            'street' => 'Street',
            'zip_code' => 'Zip Code',
            'city' => 'City',
            'account_owner' => 'Account Owner',
            'iban' => 'Iban',
            'paymentDataId' => 'Payment Data ID',
            'stage_completed' => 'Stage Completed',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UserQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['user_uuid' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by loginID
     *
     * @param string $loginID
     * @return static|null
     */
    public static function findById($loginID)
    {
        return static::findOne(['unique_login_id' => $loginID]);
    }

    /**
     * @param $uuid
     * @return User|null
     */
    public static function findByUUID($uuid)
    {
        return static::findOne(['user_uuid' => $uuid]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        return null;
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->unique_login_id = Yii::$app->security->generateRandomString(10);
    }

    /**
     * @return string
     */
    public function getAuthKey()
    {
        return $this->unique_login_id;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->unique_login_id === $authKey;
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->stage_completed = User::STAGE_COMPLETED_PERSONAL;
            $this->generateAuthKey();
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (!$insert) {
            if ($this->scenario == self::SCENARIO_PAYMENT) {
                //Todo     "message": "Missing Authentication Token" error is showing.
                 $response = $this->clientCall();
                 $data = $response->getData();
                 if (isset($data['paymentDataId'])) {
                     $this->paymentDataId = $data['paymentDataId'];
                 }
            }
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    /**
     * @return \yii\httpclient\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    private function clientCall() {
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-b ackend-dev-save-payment-data')
            ->setData(['customerId'=> 1, 'iban' => $this->iban, 'owner'=> $this->account_owner])
            ->send();
        return $response;
    }
}

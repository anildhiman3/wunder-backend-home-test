<?php

use yii\db\Migration;

/**
 * Class m210220_071519_user_table
 */
class m210220_071519_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'user_uuid'=> $this->char(60),
            'firstname' => $this->string(),
            'lastname' => $this->string(32),
            'telephone' => $this->char(10),
            'house_number' => $this->char(10),
            'street' => $this->string(100),
            'zip_code' => $this->char(10),
            'city' => $this->char(10),
            'account_owner' => $this->string(100),
            'iban' => $this->char(10),
            'paymentDataId' => $this->char(100),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
        $this->addPrimaryKey('PK', 'user', 'user_uuid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}

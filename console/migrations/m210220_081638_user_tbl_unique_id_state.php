<?php

use yii\db\Migration;

/**
 * Class m210220_081638_user_tbl_unique_id_state
 */
class m210220_081638_user_tbl_unique_id_state extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}','unique_login_id',$this->char(35)->notNull()->after('user_uuid'));
        $this->addColumn('{{%user}}','stage_completed',$this->tinyInteger(1)->defaultValue(0)->notNull()->after('paymentDataId')->comment('1-personal,2-address,3-payment'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}','unique_login_id');
        $this->dropColumn('{{%user}}','stage_completed');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210220_081638_user_tbl_unique_id_state cannot be reverted.\n";

        return false;
    }
    */
}

<?php
namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login'],
                'rules' => [
                    [
                        'actions' => ['index','personal-information','login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index','logout','personal-information','address-information','payment-information'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * save user information
     * Firstname, lastname, telephone
     * @return string|\yii\web\Response
     */
    public function actionPersonalInformation()
    {
        $model = new User();
        $model->scenario = User::SCENARIO_PERSONAL;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->stage_completed = User::STAGE_COMPLETED_PERSONAL;
            if ($model->save()) {
                if (Yii::$app->user->login(User::findById($model->unique_login_id), 3600 * 24 * 30)) {
                    Yii::$app->session->setFlash('success', 'Basic information saved successfully.');
                    return $this->redirect(['site/address-information']);
                } else {
                    Yii::$app->session->setFlash('error', 'Error while login user. Please contact with administrator');
                    return $this->refresh();
                }
            }
        } else {
            return $this->render(
                'personal-information',
                [
                    'model'=>$model
                ]
            );
        }
    }

    /**
     * save address information
     * Address including street, house number, zip code, city
     * @return string|\yii\web\Response
     */
    public function actionAddressInformation()
    {
        $model = $this->findModel();
        $model->scenario = User::SCENARIO_ADDRESS;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->stage_completed = User::STAGE_COMPLETED_ADDRESS;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Address information saved successfully. Please fill payment information');
                return $this->redirect(['site/payment-information']);
            }
        } else {
            return $this->render(
                'address-information',
                [
                    'model'=>$model
                ]
            );
        }
    }

    /**
     * * save payment information
     * Account owner , IBAN
     * @return string|\yii\web\Response
     */
    public function actionPaymentInformation()
    {
        $model = $this->findModel();
        $model->scenario = User::SCENARIO_PAYMENT;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->stage_completed = User::STAGE_COMPLETED_PAYMENT;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Payment information saved successfully.');
                return $this->redirect(['site/success']);
            }
        } else {
            return $this->render(
                'payment-information',
                [
                    'model'=>$model
                ]
            );
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->checkStageRedirect();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionSuccess()
    {
        return $this->render('success');
    }

    /**
     * @return \yii\web\Response
     */
    private function checkStageRedirect() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }
        $user = Yii::$app->user->identity;
        switch ($user->stage_completed) {
            case User::STAGE_COMPLETED_INITIAL:
                return $this->redirect(['site/personal-information']);
                break;
            case User::STAGE_COMPLETED_PERSONAL:
                return $this->redirect(['site/address-information']);
                break;
            case User::STAGE_COMPLETED_ADDRESS:
                return $this->redirect(['site/payment-information']);
                break;
            case User::STAGE_COMPLETED_PAYMENT:
                return $this->redirect(['site/success']);
                break;
            default:
                return $this->redirect(['site/address-information']);
                break;
        }
    }

    /**
     * Finds the Degree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_uuid
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel()
    {
        if (Yii::$app->user->isGuest) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (($model = User::findByUUID(Yii::$app->user->getId())) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

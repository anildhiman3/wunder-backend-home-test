<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Address information';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Please provide Address information
    </p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'address-information']); ?>

            <?= $form->field($model, 'house_number')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'street') ?>

            <?= $form->field($model, 'zip_code') ?>

            <?= $form->field($model, 'city') ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <P>PLEASE USE USERID <strong>`<?=$model->unique_login_id?>`</strong> TO LOGIN INTO SYSTEM AGAIN</P>

</div>

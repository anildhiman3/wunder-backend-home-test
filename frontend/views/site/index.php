<?php

/* @var $this yii\web\View */
use \yii\helpers\Html;
$this->title = 'User Portal';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Please select option</h1>

        <p>
            <?php
            if (!Yii::$app->user->isGuest) { ?>
                <?=Html::a('logout',['site/logout'],['data'=>['method'=>'post'],'class'=>'btn btn-lg btn-success'])?>
            <?php } else  { ?>
                <?=Html::a('New User?',['site/personal-information'],['class'=>'btn btn-lg btn-success'])?>
                <?=Html::a('Existing User?',['site/login'],['class'=>'btn btn-lg btn-success'])?>
            <?php } ?>

        </p>
    </div>
</div>

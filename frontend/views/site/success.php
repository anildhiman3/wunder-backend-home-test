<?php

/* @var $this yii\web\View */
use \yii\helpers\Html;
$user = Yii::$app->user->identity;
$this->title = 'Registration Successfully Done';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?=$this->title?></h1>
        <p>Registration Completed Successfully. Application submitted to admin for review. Will send you email for further detail</p>
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td>Name</td><td><?=$user->firstname.' '.$user->lastname?></td>
                </tr>
                <tr>
                    <td>Login ID</td><td><?=$user->unique_login_id?></td>
                </tr>
                <tr>
                    <td>Telephone</td><td><?=$user->telephone?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>
                        <?=$user->house_number?>, <?=$user->street?>, <?=$user->zip_code?>, <?=$user->city?>
                    </td>
                </tr>
                <tr>
                    <td>Account</td><td><?=$user->account_owner?></td>
                </tr>
                <tr>
                    <td>IBAN</td><td><?=$user->iban?></td>
                </tr>
                <tr><td>PaymentDataId</td><td><?=$user->paymentDataId?></td></tr>
            </tbody>
        </table>
        <?=Html::a('logout',['site/logout'],['data'=>['method'=>'post'],'class'=>'btn btn-lg btn-success'])?>
    </div>
</div>

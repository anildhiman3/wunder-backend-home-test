-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 20, 2021 at 11:14 AM
-- Server version: 5.7.32
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wunder-mobility-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1613805835),
('m210220_071519_user_table', 1613811909),
('m210220_081638_user_tbl_unique_id_state', 1613811909);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_uuid` char(60) COLLATE utf8_unicode_ci NOT NULL,
  `unique_login_id` char(35) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house_number` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_owner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iban` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentDataId` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stage_completed` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-personal,2-address,3-payment',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_uuid`, `unique_login_id`, `firstname`, `lastname`, `telephone`, `house_number`, `street`, `zip_code`, `city`, `account_owner`, `iban`, `paymentDataId`, `stage_completed`, `created_at`, `updated_at`) VALUES
('50fff5d4-7361-11eb-a979-fbf7e79f6623', 'jqhewuO2_6', 'test', 'test', '123123123', '12', 'test', 'test', 'test', 'abc', 'DIBasdf', NULL, 3, '2021-02-20 15:22:17', '2021-02-20 16:19:15'),
('60555e9e-7365-11eb-a979-fbf7e79f6623', 'ZRp0TMdxav', 't2', 't3', 't2', '44', '44', '44', '44', 'khjkh', 'khkj', NULL, 3, '2021-02-20 15:51:21', '2021-02-20 16:02:04'),
('7de2ca2c-735c-11eb-a979-fbf7e79f6623', 'S9VqmbrM-8', 'anil', 'kumar', '9041999119', '12', 'sdfsf', 'asfd', '23123', 'adfasdf', 'asdfasdf', NULL, 3, '2021-02-20 14:47:45', '2021-02-20 16:09:18'),
('a87945d6-735c-11eb-a979-fbf7e79f6623', '9fd5iLoodE', 'anil', 'kumar', '9041999119', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-02-20 14:48:56', '2021-02-20 14:48:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_uuid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
